class MPCValues:

    def __init__(self, c_mpc_ps_limit_past: float, c_mpc_soc_start: float, c_mpc_battery_capacity: float,
                 c_mpc_ocv_start: float, c_mpc_resistance: float, c_mpc_power_series: [float],
                 c_mpc_price_series_buy: [float], c_mpc_price_series_sell: [float],
                 c_mpc_soh_beginning: float):
        self.c_mpc_ps_limit_past = c_mpc_ps_limit_past
        self.c_mpc_soc_start = c_mpc_soc_start
        self.c_mpc_battery_capacity = c_mpc_battery_capacity
        self.c_mpc_ocv_start = c_mpc_ocv_start
        self.c_mpc_resistance = c_mpc_resistance
        self.c_mpc_power_series = c_mpc_power_series
        self.c_mpc_price_series_buy = c_mpc_price_series_buy
        self.c_mpc_price_series_sell = c_mpc_price_series_sell
        self.c_mpc_soh_beginning = c_mpc_soh_beginning

