import os
from configparser import ConfigParser

import pandas as pd
import numpy as np
from datetime import datetime
import webbrowser

from mpc_opt.mpc_opt_arbitrage_calendar_cyclic_oneprice import MpcOptArbitrageCalendarCyclicOnePrice
from mpc_opt.mpc_opt_arbitrage_calendar_oneprice import MpcOptArbitrageCalendarOnePrice
from mpc_opt.mpc_opt_arbitrage_oneprice import MpcOptArbitrageOnePrice


class Plotter:

    def __init__(self, df: pd.DataFrame, result_path, export_simses: bool,
                 config_opt: ConfigParser):
        self.__export_data_simses = export_simses
        self.__df: pd.DataFrame = df
        self.__results_path = result_path
        self.__optimizer_name = config_opt["GENERAL"]["OPTIMIZER"]
        self.__config_opt = config_opt

    def plot(self, time: float, name: str):
        # get figures (also updates df)
        figs = self.__plot_figures()

        # save results in specified results folder
        path = os.path.join(self.__results_path, name)
        if not (os.path.exists(path)):
            os.makedirs(path)
        all_subdirs = os.listdir(path)
        simses_subfolder = all_subdirs[-1] if self.__export_data_simses else ''
        path_plotting = os.path.join(path, simses_subfolder,
                                     'plots_MPCOpt_' + name + "_" + "{:%Y-%m-%d-%H-%M-%S}".format(datetime.now()) + '.html')
        path_csv = os.path.join(path, simses_subfolder,
                                'results_MPCOpt_' + name + "_" + "{:%Y-%m-%d-%H-%M-%S}".format(datetime.now()) + '.csv')
        path_config_opt = os.path.join(path, simses_subfolder, 'optimization' + '.ini')

        # safe opt config
        with open(path_config_opt, 'a') as f:
            self.__config_opt.write(f)
        # safe dataframe to csv
        self.__df.to_csv(path_csv)
        # create plot
        with open(path_plotting, 'a') as f:
            f.write("Simulation name is:      " + name)
            f.write("<br/>Execution time is:      " + str(round(time, 2)) + " sec")
            avg_to_create = self.__df["time_to_create"].mean()
            avg_to_solve = self.__df["time_to_solve"].mean()
            f.write("<br/>Avg. time to create model is:      " + str(round(avg_to_create, 3)) + " sec")
            f.write("<br/>Avg. time to solve model is:      " + str(round(avg_to_solve, 3)) + " sec")
            for fig in figs:
                f.write(fig.to_html(full_html=False, include_plotlyjs='cdn'))
            # print optimization config file at the bottom
            for section_name in self.__config_opt.sections():
                f.write('[%s]' % section_name)
                for name, value in self.__config_opt.items(section_name):
                    f.write('<br/> %s = %s' % (name, value))
        if self.__config_opt["GENERAL"]["PLOT_OPT"] == "True":
            webbrowser.open('file://' + path_plotting)

    def __plot_figures(self) -> []:
        # settings
        pd.options.plotting.backend = "plotly"
        template = "plotly_white"
        figs = list()
        if self.__optimizer_name in [MpcOptArbitrageCalendarOnePrice.__name__, MpcOptArbitrageOnePrice.__name__,
                                     MpcOptArbitrageCalendarCyclicOnePrice.__name__]:
            # data handling
            self.__df["revenue_buy_opt_cum"] = self.__df["revenue_buy_opt"].cumsum()
            self.__df["revenue_sell_opt_cum"] = self.__df["revenue_sell_opt"].cumsum()
            self.__df["cyclic_aging_cost_opt_cum"] = self.__df["cyclic_aging_cost_opt"].cumsum()
            self.__df["calendar_aging_cost_opt_cum"] = self.__df["calendar_aging_cost_opt"].cumsum()
            self.__df["profit_opt"] = self.__df["revenue_buy_opt_cum"] + self.__df["revenue_sell_opt_cum"]

            # calculate actual profit from energy physically bought and sold
            dt = (self.__df.index.values[1] - self.__df.index.values[0]) / np.timedelta64(1000000000, 'ns')
            self.__df["revenue_buy_sim_cum"] = -1 * self.__df["price_buy"] * self.__df["power_sim_ac_delivered"].clip(
                lower=0) * dt * 1 / 3600 * 1 / 1000000
            self.__df["revenue_buy_sim_cum"] = self.__df["revenue_buy_sim_cum"].cumsum()
            self.__df["revenue_sell_sim_cum"] = -1 * self.__df["price_sell"] * self.__df["power_sim_ac_delivered"].clip(
                upper=0) * dt * 1 / 3600 * 1 / 1000000
            self.__df["revenue_sell_sim_cum"] = self.__df["revenue_sell_sim_cum"].cumsum()
            self.__df["profit_sim"] = self.__df["revenue_buy_sim_cum"] + self.__df["revenue_sell_sim_cum"]

            figs.append(self.__df[["price_buy", "price_sell"]].plot(template=template, labels={"value": "Price [EUR/MWh]"}))

            color_map = {"revenue_buy_opt_cum": '#F5B7B1',
                         "revenue_sell_opt_cum": '#D4EFDF',
                         "profit_opt": '#D5D8DC',
                         "revenue_buy_sim_cum": '#E74C3C',
                         "revenue_sell_sim_cum": '#28B463',
                         "profit_sim": '#2C3E50'}
            figs.append(self.__df[["revenue_buy_opt_cum", "revenue_sell_opt_cum", "profit_opt", "revenue_buy_sim_cum",
                                   "revenue_sell_sim_cum", "profit_sim", "calendar_aging_cost_opt_cum",
                                   "cyclic_aging_cost_opt_cum"]].plot(template=template, line_shape="hv",
                                                                      labels={"value": "EUR", "index": "Time"},
                                                                      color_discrete_map=color_map))
            figs.append(self.__df[["revenue_buy_opt_full_horizon", "revenue_sell_opt_full_horizon",
                                   "cyclic_aging_cost_full_horizon", "calendar_aging_cost_full_horizon",
                                   "objective_value",]].plot(template=template, line_shape="hv",
                                                                      labels={"value": "EUR", "index": "Time"}))
            figs.append(
                self.__df[["time_to_solve", "time_to_create"]].plot(template=template, labels={"value": "Seconds", "index": "Time"}))
            figs.append(
                self.__df[["capacity_sim"]].plot(template=template, labels={"value": "Capacity", "index": "Time"}))
            figs.append(self.__df[["power_sim_ac_delivered", "power_opt", "power_sim_dc_delivered"]].plot(template=template, line_shape="hv",
                                                                               labels={"value": "Power / W",
                                                                                       "index": "Time"}))
            figs.append(
                self.__df[["soc_sim", "soc_opt"]].plot(template=template, labels={"value": "SoC", "index": "Time"}))
            figs.append(self.__df[["losses_charging_sim", "losses_charging_opt", "losses_discharging_sim",
                                   "losses_discharging_opt"]].plot(template=template,
                                                                   labels={"value": "Power Electornic Losses",
                                                                           "index": "Time"}))
        else:
            pass
        return figs
