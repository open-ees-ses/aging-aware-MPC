import multiprocessing
import os
import time
from configparser import ConfigParser
from datetime import datetime
from multiprocessing import Queue

from simses.commons.console_printer import ConsolePrinter
from simses.commons.utils.utilities import format_float

from main import OptSimMPC


class OptSimMPCBatchProcessing:

    """
    BatchProcessing delivers the necessary handling for running multiple parallel OptSimMPC simulations and distributes
    the configuration to each process. It supervises all processes and starts as many processes as specified.
    If you run more simulations than specified available, it waits until a simulation has finished and fills
    the gap with a new simulation processes from the queue afterwards.
    """

    UPDATE: int = 1  # s
    NUMBER_OF_PROCESSES = 5  # number of maximum parallel processes

    def __init__(self, config_set: dict, analysis_config: ConfigParser, path: str = os.getcwd()):
        # self.__max_parallel_processes: int = max(1, multiprocessing.cpu_count() - 1)
        self.__max_parallel_processes = self.NUMBER_OF_PROCESSES
        self.__path: str = os.path.join(path, 'results_batch').replace('\\','/') + '/'
        print('Results will be stored in ' + self.__path)
        self.__sim_opt_config_set: dict = config_set
        self.__analysis_config: ConfigParser = analysis_config

    def run(self):
        print(str(len(self.__sim_opt_config_set)) + ' simulations configured')
        printer_queue: Queue = Queue(maxsize=len(self.__sim_opt_config_set) * 2)
        printer: ConsolePrinter = ConsolePrinter(printer_queue)
        jobs: [OptSimMPC] = list()
        for key, value in self.__sim_opt_config_set.items():
            jobs.append(OptSimMPC(config_sim=value[0], config_opt=value[1], config_analysis=self.__analysis_config,
                                  path=self.__path, name=key))
        printer.start()
        started: [OptSimMPC] = list()
        start = time.time()
        job_count: int = len(jobs)
        count: int = 0
        for job in jobs:
            job.start()
            started.append(job)
            count += 1
            print('\rStarting simulation ' + str(count) + '/' + str(job_count) + ' @ ' + str(datetime.now()))
            self.__check_running_process(started)
        for job in jobs:
            job.join()
        duration: float = (time.time() - start) / 60.0
        print('\r\n' + type(self).__name__ + ' finished ' + str(job_count) + ' simulations in ' + format_float(duration) + ' min '
              '(' + format_float(duration / job_count) + ' min per simulation)')
        printer.stop_immediately()

    def __check_running_process(self, processes: [multiprocessing.Process]) -> None:
        while True:
            running_jobs: int = 0
            for process in processes:
                if process.is_alive():
                    running_jobs += 1
            if running_jobs < self.__max_parallel_processes:
                break
            time.sleep(self.UPDATE)


if __name__ == "__main__":

    # Construct the desired config files here and run these configfiles using the OptSimMPCBatchProcessing class to
    # create a queue of simulations and run multiple simulations in parallel.
    # This code serves as an example and may be adapted.
    # The config files from configs/.. serve as a basis and are adapted in this code.
    # This example runs 17 simulations with the different aging cost values specified in aging_cost_lists
    # with data for 2021.

    opt = "MpcOptArbitrageOnePrice"
    conf_set = {}

    list_years = [2021]
    price_csv_list = ["intraday_prices/Electricity production and spot prices in Germany 2021"]
    aging_cost_lists = [[0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]]

    for (year, price_csv, aging_cost_list) in zip(list_years, price_csv_list, aging_cost_lists):

        start_date = str(year) + "-01-01 00:00:00"
        end_date = str(year) + "-12-31 23:59:59"

        conf_sim = ConfigParser()
        conf_sim.read("configs/simulation.optsim.ini")
        conf_sim["GENERAL"]["START"] = start_date
        conf_sim["GENERAL"]["END"] = end_date
        for aging_cost in aging_cost_list:
            conf_opt = ConfigParser()
            conf_opt.read("configs/optimization.optsim.ini")
            conf_opt["GENERAL"]["OPTIMIZER"] = opt
            conf_opt["PROFILE"]["PRICE_PROFILE_MPC"] = price_csv
            conf_opt["PROFILE"]["PRICE_BUY"] = "Intraday Continuous 15 minutes ID1-Price"
            conf_opt["PROFILE"]["PRICE_SELL"] = "Intraday Continuous 15 minutes ID1-Price"
            conf_opt["GENERAL"]["AGING_COST"] = str(aging_cost)
            conf_set["aging_cost_" + "{:04d}".format(aging_cost) + "_" + str(opt) + "_" + conf_opt["PROFILE"]["PRICE_BUY"][-9:] + price_csv[-4:]] = (conf_sim, conf_opt)

    conf_analysis = ConfigParser()
    conf_analysis.read("configs/analysis.optsim.ini")

    start = time.time()
    batch_processing = OptSimMPCBatchProcessing(config_set=conf_set, analysis_config=conf_analysis)
    batch_processing.run()
    end = time.time()
    print("Execution time is: ", end-start, " seconds.")
