import multiprocessing
import os
from configparser import ConfigParser
from datetime import timedelta, datetime

from mpc_opt.mpc_opt_arbitrage_calendar_cyclic_oneprice import MpcOptArbitrageCalendarCyclicOnePrice
from mpc_opt.mpc_opt_arbitrage_calendar_oneprice import MpcOptArbitrageCalendarOnePrice
from mpc_opt.mpc_opt_arbitrage_oneprice import MpcOptArbitrageOnePrice
from plotting.plotter import Plotter
from mpc_opt.mpc_opt import MpcOpt
import pandas as pd
import numpy as np
from simses.main import SimSES
from simses.commons.state.technology.lithium_ion import LithiumIonState

from utils.utils import MPCValues
import time


class OptSimMPC(multiprocessing.Process):

    def __init__(self, path: str, name: str, config_sim: ConfigParser, config_opt: ConfigParser,
                 config_analysis: ConfigParser):

        # get config files
        super().__init__()
        self.__config_sim = config_sim
        self.__config_opt = config_opt
        self.__config_analysis = config_analysis
        self.__results_path = path
        self.__name = name
        result_folder = os.path.join(path, name)
        if not os.path.exists(result_folder):
            os.makedirs(result_folder)
        self.__export_data_simses = (self.__config_sim["GENERAL"]["export_data"] == 'True')

        # constant values for optimization (c)
        self.__c_efficiency = float(config_opt["GENERAL"]["EFFICIENCY"])
        self.__c_max_system_power = float(config_sim["STORAGE_SYSTEM"]["STORAGE_SYSTEM_AC"].split(", ")[1])  # in W
        self.__c_capacity_start = \
            float(self.__config_sim["STORAGE_SYSTEM"]["STORAGE_TECHNOLOGY"].split(", ")[1])  # in Wh
        self.__delta_t_opt = float(self.__config_opt["GENERAL"]["OPT_TIMESTEP"])
        self.__delta_t_sim = float(self.__config_sim["GENERAL"]["TIME_STEP"])

        self.__profile_df = self.__read_profiles()
        self.__results_df = pd.DataFrame()

    def run(self) -> None:
        start = time.time()
        self.run_optsim((self.__config_sim["GENERAL"]["EXPORT_DATA"] == 'True'))
        end = time.time()
        self.plotting(end-start)

    def run_optsim(self, run_simses_analysis: bool = False):
        # initialize
        simses = SimSES(path=self.__results_path, name=self.__name, simulation_config=self.__config_sim,
                        analysis_config=self.__config_analysis)
        opt: MpcOpt = self.__optimizer_factory()

        # time series handling
        timestep_dt = timedelta(seconds=self.__delta_t_opt)
        horizon = timedelta(hours=int(self.__config_opt["GENERAL"]["HORIZON"]), seconds=-self.__delta_t_opt)
        begin_dt = datetime.strptime(self.__config_sim["GENERAL"]["START"], '%Y-%m-%d %H:%M:%S')
        end_dt = datetime.strptime(self.__config_sim["GENERAL"]["END"], '%Y-%m-%d %H:%M:%S') - horizon
        number_of_loops = int(self.__config_sim["GENERAL"]["LOOP"])
        time_delta_loop = end_dt-begin_dt

        # tandem opt
        # initialize MPC values that change every simulation step
        c_mpc_soc_sim = float(self.__config_sim["BATTERY"]["START_SOC"])
        c_mpc_peak_power_sim = 0.0
        c_mpc_battery_capacity = \
            float(self.__config_sim["STORAGE_SYSTEM"]["STORAGE_TECHNOLOGY"].split(", ")[1])  # in Wh
        lithium_ion_state: LithiumIonState = simses._SimSES__storage_simulation._StorageSimulation__storage_system._StorageCircuit__storage_systems[0]._StorageSystemAC__storage_systems[0]._StorageSystemDC__storage_technology.state
        c_mpc_ocv_start = lithium_ion_state.voltage_open_circuit
        c_mpc_resistance = lithium_ion_state.internal_resistance

        # sim to opt ratio if multiple opt timesteps are simulated before optimization is run again
        sim_to_opt_ratio = int(self.__config_opt["GENERAL"]["SIM_TO_OPT_RATIO"])
        sim_opt_counter = sim_to_opt_ratio - 1
        for loop in range(0, number_of_loops):  # loop for looped simulations to repeat a given time-series
            for timestamp, row in self.__profile_df.loc[begin_dt:end_dt].iterrows():  # go through every timestep
                # time series handling
                timerange = pd.date_range(start=timestamp, end=timestamp + horizon, freq=timestep_dt)
                power_profile = self.__profile_df.loc[timerange]
                load_list = power_profile['Load'].tolist()
                price_buy_list = power_profile['Price Buy'].tolist()
                price_sell_list = power_profile['Price Sell'].tolist()
                power_profile.index = power_profile.index + loop*time_delta_loop  # adapt for looped simulations
                time_list = power_profile.index.tolist()
                timestamp_looped = timestamp + loop*time_delta_loop

                # opt: solve model with new MPC values
                if sim_opt_counter == sim_to_opt_ratio - 1:
                    mpc_values = MPCValues(c_mpc_peak_power_sim, c_mpc_soc_sim, c_mpc_battery_capacity,
                                           c_mpc_ocv_start, c_mpc_resistance, load_list, price_buy_list,
                                           price_sell_list, simses.state.soh)
                    opt.create_and_solve_model(mpc_values, time_list)
                    sim_opt_counter = 0
                else:
                    sim_opt_counter += 1

                # simses: run number of simulation timesteps with option for higher time resolution than optimization
                timsteps = self.__delta_t_opt / self.__delta_t_sim
                if not timsteps.is_integer():
                    print("WARNING: Select timesteps for optimization as multiple of timesteps of simulation.")
                else:
                    # values to be tracked from sim
                    power_sim_ac_delivered = np.zeros(int(timsteps))
                    power_sim_ac_requested = np.zeros(int(timsteps))
                    power_sim_dc_delivered = np.zeros(int(timsteps))
                    soc_sim = np.zeros(int(timsteps))
                    fullfill = np.zeros(int(timsteps))
                    sim_losses_charge = np.zeros(int(timsteps))
                    sim_losses_discharge = np.zeros(int(timsteps))

                    simses_time = timestamp_looped
                    for i in range(0, int(timsteps)):
                        simses_time += timedelta(seconds=self.__delta_t_sim)
                        # write load power into simses energymanagement state,
                        # to then use it for the SimSES analysis
                        simses._SimSES__storage_simulation._StorageSimulation__energy_management._EnergyManagement__state.load_power = load_list[0]
                        simses.run_one_simulation_step(time=simses_time.timestamp(), power=opt.power_opt[sim_opt_counter])

                        # track values from simses
                        power_sim_ac_delivered[i] = simses.state.ac_power_delivered
                        power_sim_ac_requested[i] = simses.state.ac_power
                        power_sim_dc_delivered[i] = simses.state.dc_power_intermediate_circuit
                        soc_sim[i] = simses.state.soc
                        fullfill[i] = simses.state.ac_fulfillment
                        if simses.state.ac_power_delivered > 0:
                            sim_losses_charge[i] = simses.state.pe_losses + simses.state.dc_power_loss
                            sim_losses_discharge[i] = 0
                        elif simses.state.ac_power_delivered < 0:
                            sim_losses_charge[i] = 0
                            sim_losses_discharge[i] = simses.state.pe_losses + simses.state.dc_power_loss
                        else:
                            sim_losses_charge[i] = simses.state.pe_losses + simses.state.dc_power_loss
                            sim_losses_discharge[i] = simses.state.pe_losses + simses.state.dc_power_loss

                # read SimSES state and write relevant results for this optimization step into dataframe
                data = dict(
                    load=load_list[0],
                    price_sell=price_sell_list[0],
                    price_buy=price_buy_list[0],
                    power_opt=opt.power_opt[sim_opt_counter],
                    power_sim_ac_delivered=power_sim_ac_delivered.mean(),
                    power_sim_ac_requested=power_sim_ac_requested.mean(),
                    power_sim_dc_delivered=power_sim_dc_delivered.mean(),
                    soc_opt=opt.soc_opt[sim_opt_counter],
                    soc_sim=soc_sim[-1],
                    capacity_sim=c_mpc_battery_capacity,
                    fullfill=simses.state.ac_fulfillment,
                    peak_power_sim=c_mpc_peak_power_sim,
                    peak_power_opt=opt.peak_power,
                    losses_charging_opt=opt.losses_charge[sim_opt_counter],
                    losses_discharging_opt=-opt.losses_discharge[sim_opt_counter],
                    losses_charging_sim=sim_losses_charge.mean(),
                    losses_discharging_sim=sim_losses_discharge.mean(),
                    revenue_buy_opt=opt.buying_revenue[sim_opt_counter],
                    revenue_sell_opt=opt.selling_revenue[sim_opt_counter],
                    cyclic_aging_cost_opt=opt.cyclic_aging_cost[sim_opt_counter],
                    calendar_aging_cost_opt=opt.calendar_aging_cost[sim_opt_counter],
                    revenue_buy_opt_full_horizon=opt.buying_revenue_full_horizon,
                    revenue_sell_opt_full_horizon=opt.selling_revenue_full_horizon,
                    cyclic_aging_cost_full_horizon=opt.cyclic_aging_cost_full_horizon,
                    calendar_aging_cost_full_horizon=opt.calendar_aging_cost_full_horizon,
                    objective_value=opt.objective_value,
                    time_to_solve=opt.time_solve,
                    time_to_create=opt.time_create,
                    mip_gap=opt.mip_gap
                )
                self.__results_df = pd.concat([self.__results_df, pd.DataFrame(index=[timestamp_looped], data=[data])])

                # update MPC values for next loop
                grid = load_list[0] + simses.state.ac_power_delivered
                c_mpc_peak_power_sim = grid if grid > c_mpc_peak_power_sim else c_mpc_peak_power_sim
                c_mpc_battery_capacity = simses.state.capacity  # in Wh
                c_mpc_soc_sim = simses.state.soc
                # get ocv and resistance from lithium ion state
                lithium_ion_state: LithiumIonState = \
                    simses._SimSES__storage_simulation._StorageSimulation__storage_system._StorageCircuit__storage_systems[
                        0]._StorageSystemAC__storage_systems[0]._StorageSystemDC__storage_technology.state
                c_mpc_ocv_start = lithium_ion_state.voltage_open_circuit
                c_mpc_resistance = lithium_ion_state.internal_resistance

                print('\r', timestamp_looped, end='')
        simses.close_simulation()
        if run_simses_analysis:
            simses.run_analysis()
            simses.close_analysis()

    def plotting(self, time: float):
        plotter = Plotter(self.__results_df, self.__results_path, self.__export_data_simses,
                          self.__config_opt)
        plotter.plot(time, self.__name)

    def __optimizer_factory(self) -> MpcOpt:
        optimizer_type = self.__config_opt["GENERAL"]["OPTIMIZER"]
        sim_to_opt_ratio = int(self.__config_opt["GENERAL"]["SIM_TO_OPT_RATIO"])
        begin = datetime.strptime(self.__config_sim["GENERAL"]["START"], '%Y-%m-%d %H:%M:%S')
        optimizer: MpcOpt = None
        if optimizer_type == MpcOptArbitrageCalendarOnePrice.__name__:
            aging_cost = float(self.__config_opt["GENERAL"]["AGING_COST"])
            optimizer = MpcOptArbitrageCalendarOnePrice(self.__c_efficiency, self.__c_max_system_power,
                                                        self.__delta_t_opt, sim_to_opt_ratio, aging_cost,
                                                        self.__c_capacity_start, self.__config_opt, begin)
        elif optimizer_type == MpcOptArbitrageCalendarCyclicOnePrice.__name__:
            aging_cost = float(self.__config_opt["GENERAL"]["AGING_COST"])
            optimizer = MpcOptArbitrageCalendarCyclicOnePrice(self.__c_efficiency, self.__c_max_system_power,
                                                              self.__delta_t_opt, sim_to_opt_ratio, aging_cost,
                                                              self.__c_capacity_start, self.__config_opt, begin)
        elif optimizer_type == MpcOptArbitrageOnePrice.__name__:
            aging_cost = float(self.__config_opt["GENERAL"]["AGING_COST"])
            optimizer = MpcOptArbitrageOnePrice(self.__c_efficiency, self.__c_max_system_power,
                                                self.__delta_t_opt, sim_to_opt_ratio, aging_cost,
                                                self.__c_capacity_start, self.__config_opt, begin)
        else:
            raise Exception('Optimizer with identifier  ' + optimizer_type + '  not found.')
        return optimizer

    def __read_profiles(self) -> pd.DataFrame:
        optimizer_type = self.__config_opt["GENERAL"]["OPTIMIZER"]
        begin_read = self.__config_sim["GENERAL"]["START"]
        end_read = self.__config_sim["GENERAL"]["END"]
        load = []
        price_buy = []
        price_sell = []
        index = []
        # read applicable profile for the given optimizer and fill the other profiles with zeroes
        if optimizer_type in [MpcOptArbitrageCalendarOnePrice.__name__, MpcOptArbitrageCalendarCyclicOnePrice.__name__,
                              MpcOptArbitrageOnePrice.__name__]:
            path = "data/" + self.__config_opt["PROFILE"]["PRICE_PROFILE_MPC"] + ".csv"
            full_profile = pd.read_csv(path, index_col=0, parse_dates=True, dayfirst=True)
            price_buy = full_profile.loc[begin_read:end_read, self.__config_opt["PROFILE"]["PRICE_BUY"]].tolist()
            price_sell = full_profile.loc[begin_read:end_read, self.__config_opt["PROFILE"]["PRICE_SELL"]].tolist()
            index = full_profile.loc[begin_read:end_read].index.tolist()
            load = [0] * len(price_buy)
        # build dataframe of same structure for all
        profile_df = pd.DataFrame(
            data={'Load': load,
                  'Price Sell': price_sell,
                  'Price Buy': price_buy},
            index=index)
        return profile_df


if __name__ == "__main__":
    # get config files
    conf_sim = ConfigParser()
    conf_sim.read("configs/simulation.optsim.ini")
    conf_opt = ConfigParser()
    conf_opt.read("configs/optimization.optsim.ini")
    conf_analysis = ConfigParser()
    conf_analysis.read("configs/analysis.optsim.ini")

    # name simulation, define results folder and run OptSimMPC
    start = time.time()
    path = os.path.abspath(".")
    if conf_sim["GENERAL"]["EXPORT_DATA"] == 'True':
        path = os.path.join(path, 'results').replace('\\', '/') + '/'
    else:
        path = os.path.join(path, 'results_opt_only').replace('\\', '/') + '/'
    name = "OptSimMPC1"
    optsim = OptSimMPC(path, name, conf_sim, conf_opt, conf_analysis)
    optsim.run()
    end = time.time()
    print("Execution time is: ", end-start, " seconds.")
