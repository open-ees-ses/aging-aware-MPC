# Aging aware MPC for battery energy storage systems
This open source optimization project uses a model predictive control (MPC) framework to run different optimization models on the [SimSES](https://gitlab.lrz.de/open-ees-ses/simses) simulation environment. 
The focus of the currently implemented optimization models is on aging aware operation of battery energy storage systems on the intraday electricity market.

## Use
Before using the MPC framework for the first time, follow the set-up instructions of the next section to install. A detailed description of the project structure is also available below.

To start a simulation, run:
```
python main.py
```
Or run the `main.py` file from your IDE. Instead of running individual simulations one at a time, `opt_sim_mpc_batch_processing.py`may be used to create a queue of simulations and run multiple simulations in parallel. 

The simulation settings are managed from three configuration files, which can be found under `configs/`:
1. `simulation.optsim.ini`: Used to configure the battery energy storage system and the simulation timeframe.
2. `optimization.optsim.ini`: Used to configure the optimization model and all related parameters.
3. `analysis.optsim.ini`: Used to configure the post-simulation techno-economic analysis in SimSES (optional).



## Setup and installation

### 1. Create a virtual environment
Create a virtual environment, for example with either
[venv](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) or [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html), or directly through your IDE.

### 2. Install dependencies
Install `simses` and all other required python packages in your virtual environment. This can be done with a single command with the provided "requirements" file:
```
pip install -r requirements.txt
```
Some IDEs, like [PyCharm](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#env-requirements) and [VS Code](https://code.visualstudio.com/docs/python/environments) 
may install the requirements automatically when setting up the virtual environment.

### 3. Install Gurobi
All currently implemented optimization models use the Gurobi solver and API.
Gurobi is a commercial solver, but free licenses are available for academic use. Academic licenses for Gurobi are available[ here](https://www.gurobi.com/features/academic-named-user-license/). 

## Project structure
Overview of the most relevant files and directories:
- `main.py`: Overall simulation and data handling to initialize the selected optimization model, initialize SimSES, and 
run the MPC framework. Execute this file to run the configured simulation.
- `opt_sim_mpc_batch_processing.py`: May be used to create a queue of simulations and run multiple simulations in parallel
- `configs/`: Configuration files based on which the simulation is configured
- `data/`: Intraday electricity price time series and coefficients for linearized degradation models
- `mpc_opt/`: Directory of different optimization models, all using mpc_opt.py as the parent class. All currently
implemented models use the Gurobi Python API to build and solve the models. 
  - `mpc_opt_arbitrage_oneprice.py`: Energy arbitrage with only energy throughput related aging cost
  - `mpc_opt_arbitrage_calendar_oneprice.py`: Energy arbitrage with energy throughput related aging cost plus a 
  linearized calendar aging model for a LFP cell
  - `mpc_opt_arbitrage_calendar_cyclic_oneprice.py`: Energy arbitrage with a linearized calendar and linearized cyclic 
  degradation model for a LFP cell
- `plotting/plotter.py`: Creation of plots and generating CSV files at the end of the simulation
