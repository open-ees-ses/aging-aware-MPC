from abc import ABC, abstractmethod
from utils.utils import MPCValues


class MpcOpt(ABC):

    def __init__(self):
        # relevant optimization results to be tracked
        self.power_opt = list()
        self.soc_opt = list()
        self.peak_power: float = 0
        self.losses_charge = list()
        self.losses_discharge = list()
        self.buying_revenue = list()
        self.selling_revenue = list()
        self.calendar_aging_cost = list()
        self.cyclic_aging_cost = list()

        self.buying_revenue_full_horizon = 0
        self.selling_revenue_full_horizon = 0
        self.calendar_aging_cost_full_horizon = 0
        self.cyclic_aging_cost_full_horizon = 0
        self.objective_value = 0
        self.time_create = 0
        self.time_solve = 0
        self.mip_gap = 0

    @abstractmethod
    def create_and_solve_model(self, mpc_values: MPCValues, timesteps: [float]) -> None:
        pass
