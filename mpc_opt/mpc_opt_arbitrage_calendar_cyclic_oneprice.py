import time
from configparser import ConfigParser
from datetime import datetime

import gurobipy as gp
from gurobipy import GRB
from gurobipy import quicksum

from mpc_opt.linearized_models.lfp_sony_coefficients_lin_degradation import SonyLFPCoefficientsLinearDegradation
from mpc_opt.mpc_opt import MpcOpt
from utils.utils import MPCValues


class MpcOptArbitrageCalendarCyclicOnePrice(MpcOpt):
    # Energy arbitrage with a linearized calendar and linearized cyclic degradation model for a LFP cell
    def __init__(self, c_efficiency: float, c_max_system_power: float,
                 c_delta_t: float, sim_to_opt_ratio: int, aging_cost: float,
                 capacity_start: float, config_opt: ConfigParser, begin: datetime):
        super().__init__()

        # constants (c) that stay constant for the full simulation timeframe
        self.__c_efficiency = c_efficiency
        self.__c_max_system_power = c_max_system_power  # in W
        self.__c_delta_t = c_delta_t  # in sec
        self.__c_Ws_to_MWh = 1/1000*1/1000*1/3600
        self.__c_aging_cost = aging_cost
        self.__c_capacity_start = capacity_start * 3600  # to WS
        self.__aging_cost_discount = (config_opt["GENERAL"]["DISCOUNTED_AGING_COST"] == 'True')
        self.__c_aging_cost_discount_factor = float(config_opt["GENERAL"]["DISCOUNT_FACTOR"])

        # linearizations
        self.__linear_degradation: SonyLFPCoefficientsLinearDegradation = \
            SonyLFPCoefficientsLinearDegradation(config_opt)

        self.__mip_gap = float(config_opt["GENERAL"]["MIP_GAP"])
        self.__time_limit = float(config_opt["GENERAL"]["TIME_LIMIT"])

        self.__begin: datetime = begin

        # number of optimization steps that are evaluated before optimization is called again
        self.__sim_to_opt_ratio = sim_to_opt_ratio

        # start gurobi environment at initialization to surpess output
        self.__env = gp.Env(empty=True)
        self.__env.setParam("OutputFlag", 0)
        self.__env.start()

    def create_and_solve_model(self, mpc_values: MPCValues, timesteps: [float]):
        start_time_creation = time.time()
        model = gp.Model("Arbitrage Bucket Cyclic Aging + Calendar Aging", self.__env)
        model.setParam('MIPGap', self.__mip_gap)
        model.setParam('TimeLimit', self.__time_limit)
        # MPC constants (c_mpc) that change every simulation step
        c_mpc_battery_capacity = mpc_values.c_mpc_battery_capacity * 3600  # to WS
        c_mpc_soc_start = mpc_values.c_mpc_soc_start
        electricity_price = dict(zip(timesteps, mpc_values.c_mpc_price_series_sell))   # in € per MWh

        # index sets
        coefficients_lin_deg_cal = self.__linear_degradation.get_cal_coefficients(mpc_values.c_mpc_soh_beginning)
        c_lin_x_cal = coefficients_lin_deg_cal[:, 0]
        c_lin_y_cal = coefficients_lin_deg_cal[:, 1]
        set_I_cal = range(0, int(c_lin_y_cal.shape[0]))
        set_sos_cal = {}  # dict of sos type 2 variable sets for every timestep

        coefficients_lin_deg_cyc = self.__linear_degradation.get_cyc_coefficients(mpc_values.c_mpc_soh_beginning)
        c_lin_x_cyc = coefficients_lin_deg_cyc[:, 0] * mpc_values.c_mpc_battery_capacity
        c_lin_y_cyc = coefficients_lin_deg_cyc[:, 1]
        max_energy_throughput = max(c_lin_x_cyc)
        set_J_cyc = range(0, int(c_lin_y_cyc.shape[0]))  # index set of sos-2-set of cyclic degradation
        set_H_cyc = range(0, int(((timesteps[-1].value - timesteps[0].value)/1000000000 + self.__c_delta_t) / 14400))  # index set of time set of four hours
        set_sos_cyc_dis = {}  # sos-2-set of cyclic degradation of discharging half-cycle
        set_sos_cyc_char = {}  # sos-2-set of cyclic degradation of charging half-cycle
        set_T_h_cyc = {}  # set of timesteps in each 4hr horizon

        # assign timesteps to 4hr horizons
        t_h = []
        h_cyc = 0
        t_total = 0
        for t in timesteps:
            if t_total >= 60*60*4:
                set_T_h_cyc[h_cyc] = t_h
                t_total = self.__c_delta_t
                h_cyc += 1
                t_h = [t]
            else:
                t_total += self.__c_delta_t
                t_h.append(t)
        set_T_h_cyc[h_cyc] = t_h

        # variables (v)
        v_power_charge = {}
        v_power_discharge = {}
        v_soc = {}

        v_qloss_cal = {}
        v_lambda_cal = {}

        v_qloss_cyc_char = {}
        v_qloss_cyc_dis = {}
        v_lambda_cyc_char = {}
        v_lambda_cyc_dis = {}
        v_e_sum_char = {}
        v_e_sum_dis = {}

        for t in timesteps:
            v_power_charge[t] = model.addVar(name="v_power_charge_%s" % t, vtype=GRB.CONTINUOUS, lb=0,
                                             ub=self.__c_max_system_power)
            v_power_discharge[t] = model.addVar(name="v_power_discharge_%s" % t, vtype=GRB.CONTINUOUS, lb=0,
                                             ub=self.__c_max_system_power)
            v_soc[t] = model.addVar(name="v_soc_%s" % t, vtype=GRB.CONTINUOUS, lb=0, ub=1)

            v_qloss_cal[t] = model.addVar(name="v_qloss_cal_%s" % t, vtype=GRB.CONTINUOUS, lb=0, ub=1)
            sos = []
            for i_cal in set_I_cal:
                v_lambda_cal[t, i_cal] = model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS,
                                                  name="sos-2-variable of calendar degradation_%s_%s" % (t, i_cal))
                sos.append(v_lambda_cal[t, i_cal])
            set_sos_cal[t] = sos

        for h_cyc in set_H_cyc:
            v_e_sum_char[h_cyc] = model.addVar(lb=0.0, vtype=GRB.CONTINUOUS,
                                               name="throughput of charging energy_%s" % h_cyc)
            v_e_sum_dis[h_cyc] = model.addVar(lb=0.0, vtype=GRB.CONTINUOUS,
                                              name="throughput of discharging energy_%s" % h_cyc)
            v_qloss_cyc_char[h_cyc] = model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS,
                                                   name="cyclic degradation of charging half-cycle_%s" % h_cyc)
            v_qloss_cyc_dis[h_cyc] = model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS,
                                                  name="cyclic degradation of discharging half-cycle_%s" % h_cyc)
            sos = []
            for j_cyc in set_J_cyc:
                v_lambda_cyc_char[h_cyc, j_cyc] = model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS,
                                                           name="sos-2-variable of cyclic degradation of charging_%s" % h_cyc)
                sos.append(v_lambda_cyc_char[h_cyc, j_cyc])
            set_sos_cyc_char[h_cyc] = sos

            sos = []
            for j_cyc in set_J_cyc:
                v_lambda_cyc_dis[h_cyc, j_cyc] = model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS,
                                                          name="sos-2-variable of cyclic degradation of discharging_%s" % h_cyc)
                sos.append(v_lambda_cyc_dis[h_cyc, j_cyc])
            set_sos_cyc_dis[h_cyc] = sos

        if self.__aging_cost_discount:
            t1 = time.mktime(self.__begin.timetuple())
            t2 = time.mktime(timesteps[0].to_pydatetime().timetuple())
            fractional_years = (t2 - t1) / (365 * 24 * 60 * 60)
            aging_cost = self.__c_aging_cost * (1 + self.__c_aging_cost_discount_factor)**fractional_years
        else:
            aging_cost = self.__c_aging_cost
        # objective function
        model.setObjective(
            quicksum(electricity_price[t] * self.__c_delta_t * (v_power_discharge[t] - v_power_charge[t]) * self.__c_Ws_to_MWh
                     - self.__c_capacity_start / (1000 * 3600) / (1-0.8) * aging_cost * v_qloss_cal[t]
                     for t in timesteps)
            - quicksum(self.__c_capacity_start / (1000 * 3600) / (1-0.8) * aging_cost *
                       (v_qloss_cyc_dis[h_cyc] + v_qloss_cyc_char[h_cyc]) for h_cyc in set_H_cyc),
            GRB.MAXIMIZE
        )

        # ---------------------------------------------- constraints ---------------------------------------------------
        # soc over time bucket
        for t, t_prev in zip(timesteps[1:], timesteps[:-1]):
            # bucket
            model.addConstr(v_soc[t] == v_soc[t_prev] + self.__c_delta_t / c_mpc_battery_capacity * (v_power_charge[t] * self.__c_efficiency - 1 / self.__c_efficiency * v_power_discharge[t]))
        model.addConstr(v_soc[timesteps[0]] == c_mpc_soc_start + self.__c_delta_t / c_mpc_battery_capacity * (v_power_charge[timesteps[0]] * self.__c_efficiency - 1 / self.__c_efficiency * v_power_discharge[timesteps[0]]))

        for t in timesteps:
            # max charge and discharge power
            model.addConstr(v_power_charge[t] <= self.__c_max_system_power)
            model.addConstr(v_power_discharge[t] <= self.__c_max_system_power)

            # linearization of calendar degradation
            model.addSOS(type=GRB.SOS_TYPE2, vars=set_sos_cal[t])
            model.addConstr((quicksum(v_lambda_cal[t, i_cal] for i_cal in set_I_cal) == 1), name="convexity_sos_2_cal")
            model.addConstr((quicksum(v_lambda_cal[t, i_cal] * c_lin_x_cal[i_cal] for i_cal in set_I_cal) == v_soc[t]),
                            name="soc_deg_cal")
            model.addConstr((quicksum(v_lambda_cal[t, i_cal] * c_lin_y_cal[i_cal] for i_cal in set_I_cal) == v_qloss_cal[t]),
                            name="deg_cal")

        # linearization of cyclic degradation
        for h_cyc in set_H_cyc:
            # SOS-2-set of cyclic degradation of charging half-cycle
            model.addSOS(type=GRB.SOS_TYPE2, vars=set_sos_cyc_char[h_cyc])
            model.addConstr((quicksum(v_lambda_cyc_char[h_cyc, j_cyc] for j_cyc in set_J_cyc) == 1),
                          name="convexity_sos_2_cyc_char")
            model.addConstr((quicksum(v_lambda_cyc_char[h_cyc, j_cyc] * c_lin_x_cyc[j_cyc] for j_cyc in set_J_cyc)
                           == v_e_sum_char[h_cyc]), name="sum_p_bat_char_model")
            model.addConstr((quicksum(v_lambda_cyc_char[h_cyc, j_cyc] * c_lin_y_cyc[j_cyc] for j_cyc in set_J_cyc)
                           == v_qloss_cyc_char[h_cyc]), name="deg_cyc_char")
            # Throughput of charging energy in every timestep of four hours
            model.addConstr(quicksum(v_power_charge[t] * self.__c_delta_t / 3600 for t in set_T_h_cyc[h_cyc])
                            == v_e_sum_char[h_cyc], name="sum_p_bat_char")

            # SOS-2-set of cyclic degradation of discharging half-cycle
            model.addSOS(type=GRB.SOS_TYPE2, vars=set_sos_cyc_dis[h_cyc])
            model.addConstr((quicksum(v_lambda_cyc_dis[h_cyc, j_cyc] for j_cyc in set_J_cyc) == 1),
                          name="convexity_sos_2_cyc_dis")
            model.addConstr((quicksum(v_lambda_cyc_dis[h_cyc, j_cyc] * c_lin_x_cyc[j_cyc] for j_cyc in set_J_cyc) == v_e_sum_dis[h_cyc]),
                          name="sum_p_bat_dis_model")
            model.addConstr((quicksum(v_lambda_cyc_dis[h_cyc, j_cyc] * c_lin_y_cyc[j_cyc] for j_cyc in set_J_cyc)
                           == v_qloss_cyc_dis[h_cyc]), name="deg_cyc_dis")
            # Throughput of discharging energy in every timestep of four hours
            model.addConstr(quicksum(v_power_discharge[t] * self.__c_delta_t / 3600 for t in set_T_h_cyc[h_cyc])
                            == v_e_sum_dis[h_cyc], name="sum_p_bat_dis")

        # optimize model and log time to solve
        start_time_optimization = time.time()
        model.optimize()
        end_time = time.time()

        # log data
        self.time_solve = end_time - start_time_optimization
        self.time_create = start_time_optimization - start_time_creation

        self.power_opt = [v_power_charge[timesteps[i]].x - v_power_discharge[timesteps[i]].x for i in range(0, self.__sim_to_opt_ratio)]
        self.soc_opt = [v_soc[timesteps[i]].x for i in range(0, self.__sim_to_opt_ratio)]
        self.losses_charge = [v_power_charge[timesteps[i]].x * (1-self.__c_efficiency) for i in range(0, self.__sim_to_opt_ratio)]
        self.losses_discharge = [(1 / self.__c_efficiency-1) * v_power_discharge[timesteps[i]].x for i in range(0, self.__sim_to_opt_ratio)]

        buying_revenue = [-v_power_charge[t].x * electricity_price[t] * self.__c_delta_t * self.__c_Ws_to_MWh for t in timesteps]
        self.buying_revenue = buying_revenue[0: self.__sim_to_opt_ratio]
        self.buying_revenue_full_horizon = sum(buying_revenue)

        selling_revenue = [v_power_discharge[t].x * electricity_price[t] * self.__c_delta_t
                                * self.__c_Ws_to_MWh for t in timesteps]
        self.selling_revenue = selling_revenue[0: self.__sim_to_opt_ratio]
        self.selling_revenue_full_horizon = sum(selling_revenue)

        calender_aging_cost = [c_mpc_battery_capacity / (1000 * 3600) / (1-0.8) * self.__c_aging_cost *
                                    v_qloss_cal[t].x for t in timesteps]
        self.calendar_aging_cost = calender_aging_cost[0:self.__sim_to_opt_ratio]
        self.calendar_aging_cost_full_horizon = sum(calender_aging_cost)

        cyclic_aging_cost = [c_mpc_battery_capacity / (1000 * 3600) / (1 - 0.8) * self.__c_aging_cost *
                 (v_qloss_cyc_dis[h_cyc].x + v_qloss_cyc_char[h_cyc].x) for h_cyc in set_H_cyc]
        # h_cyc has different length from timesteps, therefore log average
        self.cyclic_aging_cost = [sum(cyclic_aging_cost) / len(timesteps) * self.__sim_to_opt_ratio for x in range(0, self.__sim_to_opt_ratio)]
        self.cyclic_aging_cost_full_horizon = sum(cyclic_aging_cost)

        self.mip_gap = model.MIPGap

        self.objective_value = model.objVal

        # print("Pbatt and SOC over time:")
        # for t in timesteps:
        #     print(electricity_price[t], v_power_charge[t], v_power_discharge[t], v_soc[t])
        # x = 1

        # for t in timesteps:
        #     if (v_power_charge[t].x != 0) and (v_power_discharge[t].x != 0):
        #         print(t, electricity_price[t], v_power_charge[t], v_power_discharge[t], v_soc[t])
